#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <mtd/mtd-user.h>

#include "common.h"
#include "utils.h"
#include "flash.h"

#define DEFAULT_CTRL_DEV    "/dev/ubi_ctrl"

static struct flash_description flashdesc;

static int flash_ubi_open(void)
{
    flashdesc.libubi = libubi_open();

	if (flashdesc.libubi == NULL) {
        return -1;
	}
	
    return 0;
}

static int flash_ubi_init(void)
{
    int ret = 0;

    if ((ret = flash_ubi_open()) != 0) {
        if (errno == 0) {
			printf("UBI is not present in the system \n");
        } else {
		    printf("cannot open libubi - %s \n", strerror(errno));
        }
        return -1;
    }
    
    printf("UBI open, getting info \n");
    if ((ret = ubi_get_info(flashdesc.libubi, &flashdesc.ubiinfo)) != 0) {
	    printf("Failed to get UBI info \n");
        return -1;
    }

    printf("There are %d UBI devices\n", flashdesc.ubiinfo.dev_count);

    return 0;
}

static int flash_mtd_open(void)
{
    flashdesc.libmtd = libmtd_open();

	if (flashdesc.libmtd == NULL) {
        return -1;
	}
	
    return 0;
}

static int flash_mtd_init(void)
{
    int ret = 0;

    if ((ret = flash_mtd_open()) != 0) {
        if (errno == 0) {
			printf("MTD is not present in the system \n");
        } else {
		    printf("cannot open libmtd - %s \n", strerror(errno));
        }
        return -1;
    }

    printf("MTD open, getting info \n");
    if ((ret = mtd_get_info(flashdesc.libmtd, &flashdesc.mtdinfo)) != 0) {
	    printf("Failed to get MTD info \n");
        return -1;
    }

    printf("Highest: %d, lowest: %d \n", flashdesc.mtdinfo.highest_mtd_num, 
            flashdesc.mtdinfo.lowest_mtd_num);

    return 0;
}

static void flash_erase_buffer(void *buffer, size_t size)
{
	const uint8_t eraseByte = 0xff;

    printf("Erase size %d \n", size);

	if (buffer != NULL && size > 0)
		memset(buffer, eraseByte, size);
}

int flash_init(void)
{
    memset(&flashdesc, 0, sizeof(struct flash_description));
    if (flash_mtd_init() != 0) {
        return -1;
    }
    if (flash_ubi_init() != 0) {
        return -1;
    }
    
    return 0;
}

int flash_nand_init_update(struct image *img)
{
    int ret;
    int fd;
    int pagelen;
    int cur_eb          = 0;
    int cur_mtd_offset  = 0;
    int writebuf_max    = 0;
    long long imglen;
    long long cur_block_start = -1;
    long long offset;
    unsigned char *writebuf     = NULL;
    unsigned char *writemark    = NULL;
    unsigned char *cur          = NULL;
    unsigned char *writeend     = NULL;
    char fout[32];
    struct mtd_dev_info info;

    ret = mtd_get_dev_info1(flashdesc.libmtd, img->mtdnum, &info);
    if (ret != 0) {
        printf("Failed to get MTD info for %d \n", img->mtdnum);
        return -1;
    }

    pagelen = info.min_io_size;
    imglen = img->src_size;

    writebuf_max = info.eb_size / info.min_io_size * pagelen;
    printf("creating writebuf of %d size \n", writebuf_max);
    writebuf = calloc(1, writebuf_max);
    writemark = writebuf;
    writeend = (writebuf + writebuf_max);
    flash_erase_buffer(writebuf, writebuf_max);

    printf("MTD info for %s, erase blocks = %d, erase size %d, page = %d, size = %lld \n", 
            info.name, info.eb_cnt, info.eb_size, pagelen, info.size);

    // set the image file offset in the CPIO archive
    if (lseek(img->src_fd, img->src_offset, SEEK_SET) == -1) {
		printf("Unable to seek offet on CPIO archive : %s\n", strerror(errno));
        return -1;
	}

    // open MTD device,
    sprintf(fout, "/dev/mtd%d", img->mtdnum);

    fd = open(fout, O_RDWR);
    if (fd < 0) {
        printf("Unable to open %s - %s \n", fout, strerror(errno));
        return -1;
    }

    for (cur_eb = 0; cur_eb < info.eb_cnt; cur_eb++) {
        ret = mtd_erase(flashdesc.libmtd, &info, fd, cur_eb);
        if (ret != 0) {
            printf("Failed to erase block %d on %s \n", cur_eb, fout);
        }
    }

    while (imglen > 0 || cur_eb > info.eb_cnt) {
        // fill write buffer
        if (writemark < (writebuf + writebuf_max)) {
            //printf("we have room in the write buffer - %d \n", (writeend - writemark));
            cur = writemark;
            size_t cnt = 0;

            while (imglen > 0 && cur < writeend) {
                size_t read_size = imglen > pagelen ? pagelen: imglen;
                cnt = read(img->src_fd, cur, read_size);
                //printf("expected read %d, got %d \n", read_size, cnt);
                imglen -= cnt;

                if (cnt < pagelen) {
                    printf("short read - %d, need to pad buffer, imglen %d \n", cnt, imglen);
                    flash_erase_buffer(cur + cnt, ((writeend - writemark) - cnt));
                    writemark += pagelen;
                    break;
                }
                if (cnt == 0) { /* EOF */
                    printf("nothing to read \n");
                    imglen = 0;
                    writemark = writebuf;
                    break;
                } else if(cnt == -1) {
                    printf("IO error - %s \n", strerror(errno));
                    goto closeall;
                }
                writemark += pagelen;
                cur += pagelen;
            }
        }
        
        while (cur_block_start != (cur_mtd_offset & (~info.eb_size + 1))) {
           cur_block_start = cur_mtd_offset & (~info.eb_size + 1);
           offset = cur_block_start;

           do {
               ret = mtd_is_bad(flashdesc.libmtd, fd, offset / info.eb_size);
               if (ret < 0) {
                   printf("mtd%d: MTD is bad blocked failed \n", img->mtdnum);
                   return -1;
               } else if (ret == 1) {
                   // this is a bad block
                   printf("mtd%d: block %d is bad \n", img->mtdnum, cur_eb);
                   cur_mtd_offset = cur_block_start + info.eb_size;
                   if (cur_mtd_offset > info.size) {
                       printf("Mtd%d has too many bad blocks, cannot continue \n", img->mtdnum);
                       goto closeall;
                   }
               }
               offset += info.eb_size;
           } while (offset < cur_block_start + info.eb_size);
        }
        
        // write to flash
        cur = writebuf;
        while (cur < writemark) {
            //printf("Writing to eb %d, to offset %d \n", (cur_mtd_offset / info.eb_size), 
            //    (cur_mtd_offset % info.eb_size));

            ret = mtd_write(flashdesc.libmtd, &info, fd, (cur_mtd_offset / info.eb_size),
                    (cur_mtd_offset % info.eb_size), cur, pagelen, 
                    NULL, 0, MTD_OPS_PLACE_OOB);
            if (ret == -1) {
                printf("Error writing to eb offset, rewind write buffer\n");
                break;
            }

            cur_mtd_offset += pagelen;
            cur += pagelen;
        }

        if (cur == writemark) {
            //printf("write up to watermark, resetting\n");
            writemark = writebuf;
        } else {
            printf("not good, possible bad offset in errase block\n");
        }

        //printf("Image len %lld \n", imglen);
    }

closeall:
    free(writebuf);
    close(fd);

    return 0;
}

int flash_ubi_init_update(int mtdnum, const char *volume, long long bytes)
{
    int ret, fd;
    char fout[32];
    struct ubi_dev_info info;
    struct ubi_vol_info vol;

    flashdesc.request.dev_num = UBI_DEV_NUM_AUTO;
    flashdesc.request.mtd_num = mtdnum;
    flashdesc.request.vid_hdr_offset = 0;
    flashdesc.request.mtd_dev_node = NULL;
    flashdesc.request.max_beb_per1024 = 0;

    ret = ubi_attach(flashdesc.libubi, DEFAULT_CTRL_DEV, &flashdesc.request);
    if (ret != 0) {
        printf("Error attaching UBI %d - %s \n",ret, strerror(errno));
        return -1;
    }

    printf("UBI attahed \n");

    ret = ubi_get_dev_info1(flashdesc.libubi, flashdesc.request.dev_num, &info);
    if (ret != 0) {
        printf("Error getting UBI dev info - %s \n", strerror(errno));
        return -1;
    }

    printf("Vol count = %d, erase blocks = %d \n", info.vol_count, 
            info.total_lebs);

    ret = ubi_get_vol_info1_nm(flashdesc.libubi, info.dev_num, volume, &vol);
    if (ret != 0) {
        printf("Error getting UBI vol info - %s \n", strerror(errno));
        return -1;
    }

    printf("Vol name = %s, dev num = %d, volid = %d, vol size = %d \n", vol.name, 
            vol.dev_num, vol.vol_id, vol.leb_size);
    sleep(1);

    // we expect to have a volume, check here
    if (strcmp(vol.name, volume) != 0) {
        printf("Missing volume. expected: %d, found: %d \n", volume, vol.name);
    }

    // open UBI device,
    sprintf(fout, "/dev/ubi%d_%d", vol.dev_num, vol.vol_id);

    fd = open(fout, O_WRONLY);
    if (fd < 0) {
        printf("Unable to open %s - %s \n", fout, strerror(errno));
        return -1;
    }

    ret = ubi_update_start(flashdesc.libubi, fd, bytes);
    if (ret != 0) {
        printf("UBI update start failed - %s \n", strerror(errno));
        return -1;
    }

    return fd;
}

__attribute__((destructor))
void flash_mtd_cleanup(void) {
    if (flashdesc.libmtd == NULL) {
        return;
    }
    
    if (flashdesc.request.mtd_num > 0) {
        printf("Detaching MTD %d \n", flashdesc.request.mtd_num);
        sleep(1);
        ubi_detach_mtd(flashdesc.libubi, DEFAULT_CTRL_DEV, flashdesc.request.mtd_num);
    }

    printf("Closing MTD \n");
    libmtd_close(flashdesc.libmtd);
}
