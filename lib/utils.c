/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 * 
 * (C) Copyright 2013
 * Stefano Babic, DENX Software Engineering, sbabic@denx.de.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
 
 #include "utils.h"
 
#define BUF_SIZE	 16384

static int utils_copy_write(int fd, const void *buf, int len)
{
	int ret;

	while (len) {
		errno = 0;
		ret = write(fd, buf, len);
		if (ret < 0) {
			if (errno == EINTR)
				continue;
			printf("Cannot write %d bytes - %s \n", len, strerror(errno));
			return -1;
		}

		if (ret == 0) {
			printf("Zero bytes written - %d \n", len);
			return -1;
		}

		len -= ret;
		buf += ret;
	}

	return 0;
}

int utils_fill_buffer(int fd, unsigned char *buf, int nbytes, unsigned long *offs)
{
	unsigned long len;
	unsigned long count = 0;
	int i;

	while (nbytes > 0) {
		len = read(fd, buf, nbytes);
		if (len < 0) {
			printf("Failure in stream: I cannot go on\n");
			return -EFAULT;
		}
		if (len == 0) {
			//printf("%s: Zero read\n", __FUNCTION__);
			return 0;
		}

		buf += len;
		count += len;
		nbytes -= len;
		*offs += len;
	}

	return count;
}

int utils_copyfile(int fdin, int fdout, int nbytes, unsigned long *offset)
{
	unsigned long size;
    unsigned char buf[BUF_SIZE];
	int ret;

	while (nbytes > 0) {
		size = (nbytes < BUF_SIZE ? nbytes : BUF_SIZE);

		if ((ret = utils_fill_buffer(fdin, buf, size, offset) < 0)) {
			close(fdout);
			return ret;
		}

		nbytes -= size;
        
		if (utils_copy_write(fdout, buf, size) < 0) {
			close(fdout);
			return -ENOSPC;
		}
	}

	return 0;
}
uintmax_t utils_from_ascii(char const *where, size_t digs, unsigned logbase)
{
	uintmax_t value = 0;
	char const *buf = where;
	char const *end = buf + digs;
	int overflow = 0;
	static char codetab[] = "0123456789ABCDEF";

	for (; *buf == ' '; buf++)
	{
		if (buf == end)
		return 0;
	}

	if (buf == end || *buf == 0)
		return 0;
	while (1)
	{
		unsigned d;

		char *p = strchr (codetab, toupper (*buf));
		if (!p)
		{
			printf("Malformed number %.*s\n", (int)digs, where);
			break;
		}

		d = p - codetab;
		if ((d >> logbase) > 1)
		{
			printf("Malformed number %.*s\n", (int)digs, where);
			break;
		}
		value += d;
		if (++buf == end || *buf == 0)
			break;
		overflow |= value ^ (value << logbase >> logbase);
		value <<= logbase;
	}
	if (overflow)
		printf("Archive value %.*s is out of range\n",
			(int)digs, where);
	return value;
}

image_type utils_covert_to_image_type(const char *t)
{ 
    if (strcmp("raw", t) == 0) {
        return IMAGE_TYPE_RAW;
    }
    if (strcmp("ubifs", t) == 0) {
        return IMAGE_TYPE_UBIFS;
    }
    if (strcmp("vfat", t) == 0) {
        return IMAGE_TYPE_VFAT;
    }
    if (strcmp("nand", t) == 0) {
        return IMAGE_TYPE_NAND;
    }

    return IMAGE_TYPE_UNKNOWN;
}

