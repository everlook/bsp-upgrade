/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "cpio_utils.h"
#include "utils.h"

static int cpio_utils_get_cpiohdr(unsigned char *buf, long *size, long *namesize, long *chksum)
{
	struct new_ascii_header *cpiohdr;

	cpiohdr = (struct new_ascii_header *)buf;
	if (strncmp(cpiohdr->c_magic, "070702", 6) != 0) {
		printf("CPIO Format not recognized: magic not found\n");
			return -EINVAL;
	}
	*size = FROM_HEX(cpiohdr->c_filesize);
	*namesize = FROM_HEX(cpiohdr->c_namesize);
	*chksum =  FROM_HEX(cpiohdr->c_chksum);

	return 0;
}
 
static int cpio_utils_extract_header(int fd, struct filehdr *fhdr, unsigned long *offset)
{
    unsigned char buf[256];
    
	if (utils_fill_buffer(fd, buf, sizeof(struct new_ascii_header), offset) < 0) {
		return -EINVAL;
    }
    
	if (cpio_utils_get_cpiohdr(buf, &fhdr->filesize, &fhdr->namesize, &fhdr->chksum) < 0) {
		return -EINVAL;
	}
    
	if (fhdr->namesize >= sizeof(fhdr->filename)) {
		printf("CPIO Header filelength too big %u >= %u (max)",
			(unsigned int)fhdr->namesize,
			(unsigned int)sizeof(fhdr->filename));
		return -EINVAL;
	}

	if (utils_fill_buffer(fd, buf, fhdr->namesize , offset) < 0) {
		return -EINVAL;
    }
    
	strncpy(fhdr->filename, (char *)buf, sizeof(fhdr->filename));

	/* Skip filename padding, if any */
	if (utils_fill_buffer(fd, buf, (4 - (*offset % 4)) % 4, offset) < 0) {
		return -EINVAL;
    }

	return 0;
}

off_t cpio_utils_extract_sw_description(int fd, const char *descfile, off_t start)
{
    struct filehdr fdh;
	unsigned long offset = start;
	char output_file[64];
	uint32_t checksum;
	int fdout;

	if (cpio_utils_extract_header(fd, &fdh, &offset)) {
		printf("CPIO Header wrong\n");
		return -1;
	}

	if (strcmp(fdh.filename, descfile)) {
		printf("Expected %s but found %s.\n",
			descfile,
			fdh.filename);
		return -1;
	}

	if ((strlen(TMPDIR) + strlen(fdh.filename)) > sizeof(output_file)) {
		printf("File Name too long : %s\n", fdh.filename);
		return -1;
	}

	strncpy(output_file, TMPDIR, sizeof(output_file));
	strcat(output_file, fdh.filename);
	fdout = open(output_file, O_CREAT | O_WRONLY | O_TRUNC,  S_IRUSR | S_IWUSR );
    if (fdout < 0) {
		printf("I cannot open %s %d\n", output_file, errno);
        return -1;
    }

	if (lseek(fd, offset, SEEK_SET) < 0) {
		printf("CPIO file corrupted : %s\n", strerror(errno));
		return -1;
	}

	if (utils_copyfile(fd, fdout, fdh.filesize, &offset) < 0) {
		printf("%s corrupted or not valid\n", descfile);
		return -1;
	}

	close(fdout);

	return offset;
}

int cpio_utils_find_img(int fd, struct image *img, off_t start)
{
    struct filehdr fdh;
	unsigned long offset = start;

    //printf("Looking for %s starting at offset %d \n", img->filename, offset);
    while (1) {
        
        if (lseek(fd, offset, SEEK_SET) < 0) {
		    printf("CPIO seek error: %s\n", strerror(errno));
		    return -1;
	    }
    
	    if ((cpio_utils_extract_header(fd, &fdh, &offset)) < 0) {
		    printf("CPIO Header wrong\n");
		    return -1;
	    }

        //printf("image %s is %d bytes, new offset %d \n", fdh.filename, fdh.filesize, offset);
        if (strcmp(fdh.filename, img->filename) == 0) {
            img->src_offset = offset;
            img->src_size = fdh.filesize;
            break;
        }
        offset += fdh.filesize;
    }

    return 0;
}
