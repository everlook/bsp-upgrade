#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "notifiers.h"

#define PSPLASH_FILE "/tmp/psplash_fifo"
#define MAX_MSG_SIZE 128

static int fd = 0;
static char msg_buf[MAX_MSG_SIZE];
static char prg_buf[MAX_MSG_SIZE];

void psplash_notifier(void);

int psplash_notifier_handler(const char *msg, int progress)
{
    if (fd < 0) {
        printf("Psplash fifo %s not available, %d \n", PSPLASH_FILE, fd);
        return -1;
    }
    
    memset(msg_buf, 0, sizeof(msg_buf));
    memset(prg_buf, 0, sizeof(prg_buf));

    sprintf(msg_buf, "MSG %s\n", msg);
    write(fd, msg_buf, strlen(msg_buf));
    //TODO: without this delay some psplash messages are corrupt, maybe check psplash code
    usleep(200 * 1000);
    
    sprintf(prg_buf, "PROGRESS %d\n", progress);
    write(fd, prg_buf, strlen(prg_buf));
    //TODO: without this delay some psplash messages are corrupt, maybe check psplash code
    usleep(200 * 1000);

    return 0;
}

__attribute__((constructor))
void psplash_notifer_constructor(void) {
    fd = open(PSPLASH_FILE, O_WRONLY);
    if (fd < 0) {
        printf("Unable to open %s - %s \n", PSPLASH_FILE, strerror(errno));
        return;
    }
    
    notifier_register("psplash", psplash_notifier_handler);
}

__attribute__((destructor))
void psplash_notifer_destructor(void) {
    if (fd > 0) {
        close(fd);
    }
}

