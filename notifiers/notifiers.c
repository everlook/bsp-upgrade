#include <stdio.h>
#include <string.h>

#include "notifiers.h"

#define MAX_NOTIFIERS 8
struct progress_notifier supported_notifiers[MAX_NOTIFIERS];
static unsigned int nr_notifiers = 0;

void notifier_notify(const char *name, int progress)
{
    unsigned int i;

    if (nr_notifiers == 0) {
        return;
    }

    for (i = 0; i < nr_notifiers; i++) {
        supported_notifiers[i].notify(name, progress);
    }

}

int notifier_register(const char *name, notifier notify)
{
    if (nr_notifiers > MAX_NOTIFIERS - 1) {
        return -1;
    }
    
    strncpy(supported_notifiers[nr_notifiers].name, name, 
            sizeof(supported_notifiers[nr_notifiers].name));
    supported_notifiers[nr_notifiers].notify = notify;

    nr_notifiers++;

    return 0;
}

void notifier_print_regisered(void)
{
    unsigned int i;

    if (nr_notifiers == 0) {
        return;
    }
        
    printf("Registered notifiers \n");
    for (i = 0; i < nr_notifiers; i++) {
        printf("\t%s \n", supported_notifiers[i].name);
    }
}
