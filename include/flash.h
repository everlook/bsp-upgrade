/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#ifndef _FLASH_H
#define _FLASH_H

#include <stdint.h>
#include <mtd/libmtd.h>
#include <mtd/libubi.h>

#include "common.h"
#include "bsdqueue.h"

struct flash_description {
	libmtd_t libmtd;
	libubi_t libubi;
	struct mtd_info mtdinfo;
    struct ubi_info ubiinfo;
    struct ubi_attach_request request;
};

int flash_init(void);
int flash_ubi_init_update(int mtdnum, const char *volume, long long bytes);
int flash_nand_init_update(struct image *img);

#endif
