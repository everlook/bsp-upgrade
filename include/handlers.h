/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#ifndef HANDLERS_H
#define HANDLERS_H

typedef int (*handler)(struct image *img, void *data);

struct image_handler {
    char name[64];
    handler installer;
    void *data;
};

int handler_register(const char *name, handler installer, void *data);

struct image_handler *handler_find(struct image *img);

void handler_print_regisered(void);

#endif
