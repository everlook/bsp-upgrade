/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#ifndef NOTIFIERS_H
#define NOTIFIERS_H

typedef int (*notifier)(const char *msg, int progress);
typedef int (*notifier_init)(void);

struct progress_notifier {
    char name[64];
    notifier notify;
};

int notifier_register(const char *name, notifier notify);
void notifier_notify(const char *name, int progress);

void notifier_print_regisered(void);

#endif
