/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#ifndef _CPIOUTILS_H
#define _CPIOUTILS_H

#include <sys/types.h>

#include "common.h"

struct new_ascii_header
{
  char c_magic[6];
  char c_ino[8];
  char c_mode[8];
  char c_uid[8];
  char c_gid[8];
  char c_nlink[8];
  char c_mtime[8];
  char c_filesize[8];
  char c_dev_maj[8];
  char c_dev_min[8];
  char c_rdev_maj[8];
  char c_rdev_min[8];
  char c_namesize[8];
  char c_chksum[8];
};

struct filehdr {
	long filesize;
	long namesize;
	long chksum;
    off_t fileoffset;
	char filename[MAX_IMAGE_FNAME];
};

int cpio_utils_find_img(int fd, struct image *img, off_t start);
off_t cpio_utils_extract_sw_description(int fd, const char *descfile, off_t start);

#endif
