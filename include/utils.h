/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#ifndef _UTILS_H
#define _UTILS_H

#include <stdint.h>

#include "common.h"

#define LG_16 4
#define FROM_HEX(f) utils_from_ascii (f, sizeof f, LG_16)
uintmax_t utils_from_ascii(char const *where, size_t digs, unsigned logbase);

image_type utils_covert_to_image_type(const char *image);

int utils_fill_buffer(int fd, unsigned char *buf, int nbytes, unsigned long *offs);
int utils_copyfile(int fdin, int fdout, int nbytes, unsigned long *offset);

#endif
