/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#ifndef _COMMON_H
#define _COMMON_H

#include <sys/types.h>

#include "bsdqueue.h"

#define TMPDIR		"/tmp/"
#define SW_DESCRIPTION_FILENAME	"sw-description"
#define SW_DESCRIPTION_PATH	(TMPDIR SW_DESCRIPTION_FILENAME)

#define SWUPDATE_GENERAL_STRING_SIZE 256
#define MAX_IMAGE_FNAME SWUPDATE_GENERAL_STRING_SIZE

typedef enum {
    IMAGE_TYPE_RAW = 0,
    IMAGE_TYPE_UBIFS,
    IMAGE_TYPE_VFAT,
    IMAGE_TYPE_NAND,
    IMAGE_TYPE_UNKNOWN
} image_type;

typedef enum {
    TARGET_MMC = 0,
    TARGET_MTD
} target_type;

struct image {
     char filename[SWUPDATE_GENERAL_STRING_SIZE];
     char device[SWUPDATE_GENERAL_STRING_SIZE];
     char volume[SWUPDATE_GENERAL_STRING_SIZE];
     image_type type;
     int src_fd;
     off_t src_offset;
     long long src_size;
     unsigned short int mtdnum;
     LIST_ENTRY(image) next;
};

LIST_HEAD(imagelist, image);

struct swupgrade_cfg {
    char name[SWUPDATE_GENERAL_STRING_SIZE];
    char version[SWUPDATE_GENERAL_STRING_SIZE];
    target_type target;
    struct imagelist images;
    int image_count;
};

#endif
