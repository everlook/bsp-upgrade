#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <libconfig.h>

#include "parser.h"
#include "common.h"
#include "utils.h"
#include "bsdqueue.h"

static int parser_get_mmc_config(struct swupgrade_cfg *swcfg, config_setting_t *elem)
{
    const char *fname, *fdevice, *ftype;

    config_setting_lookup_string(elem, "filename", &fname);
    config_setting_lookup_string(elem, "device", &fdevice);
    config_setting_lookup_string(elem, "type", &ftype);
    
    //TODO: need to check length of input 
    struct image *img = (struct image *)calloc(1, sizeof(struct image));
    strncpy(img->filename, fname, strlen(fname));
    strncpy(img->device, fdevice, strlen(fdevice));
    img->type = utils_covert_to_image_type(ftype);
    
    LIST_INSERT_HEAD(&swcfg->images, img, next);

    return 0;
}

static int parser_get_mtd_config(struct swupgrade_cfg *swcfg, config_setting_t *elem)
{
    const char *fname, *ftype, *fvol;
    int mtdnum;

    config_setting_lookup_string(elem, "filename", &fname);
    config_setting_lookup_int(elem, "mtdnum", &mtdnum);
    config_setting_lookup_string(elem, "type", &ftype);
    config_setting_lookup_string(elem, "volume", &fvol);
    
    //TODO: need to check length of input 
    struct image *img = (struct image *)calloc(1, sizeof(struct image));
    strncpy(img->filename, fname, strlen(fname));
    img->type = utils_covert_to_image_type(ftype);
    img->mtdnum = mtdnum;
    // ubifs images have a volume name
    if (fvol != NULL) {
        strncpy(img->volume, fvol, strlen(fvol));
    }
    
    LIST_INSERT_HEAD(&swcfg->images, img, next);

    return 0;
}

int parser_parse_cfg(struct swupgrade_cfg *swcfg, const char *filename)
{
    config_t cfg;
    config_setting_t *setting;
    const char *str;
    int i;
    
    memset(&cfg, 0, sizeof(cfg));

    config_init(&cfg);

    if (config_read_file(&cfg, filename) != CONFIG_TRUE) {
        printf("%s:%d - %s \n", config_error_file(&cfg), 
                config_error_line(&cfg), config_error_text(&cfg));
        
        config_destroy(&cfg);

        return -1;
    }
    
    if (config_lookup_string(&cfg, "version", &str)) {
        strncpy(swcfg->version, str, sizeof(swcfg->version));
    } else {
        printf("Version identifier not found! \n");
    }

    switch (swcfg->target) {
        case TARGET_MTD:
            setting = config_lookup(&cfg, "mtd.images");break;
        case TARGET_MMC:
            setting = config_lookup(&cfg, "mmc.images");break;
        default:
            printf("Invalid target: %d \n", swcfg->target);
            return -1;
    }

    if (setting != NULL) {
        int count = config_setting_length(setting);
        for (i = 0; i < count; i++) {
            config_setting_t *elem = config_setting_get_elem(setting, i);

            if (swcfg->target == TARGET_MMC) {
               parser_get_mmc_config(swcfg, elem); 
            } else if (swcfg->target == TARGET_MTD) {
               parser_get_mtd_config(swcfg, elem); 
            } else {
                printf("Invalid boot mode! \n");
            }
        }
    } else {
        printf("Node images not found! \n");
    }

    config_destroy(&cfg);

    return 0;
}

void parser_cleanup (struct swupgrade_cfg *swcfg, const char *filename)
{
    struct image *p;

    LIST_FOREACH(p, &swcfg->images, next) {
        LIST_REMOVE(p, next);
        free(p);
    }

    unlink(filename);
}
