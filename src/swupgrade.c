/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <getopt.h>
#include <string.h>
#include <config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "common.h"
#include "cpio_utils.h"
#include "gpio_utils.h"
#include "bsdqueue.h"
#include "parser.h"
#include "handlers.h"
#include "notifiers.h"
#include "flash.h"

static struct option long_options[] = {
	{"image",   required_argument,  NULL, 'i'},
	{"help",    no_argument,        NULL, 'h'},
	{"verbose", no_argument,        NULL, 'v'},
	{"check",   no_argument,        NULL, 'c'},
	{NULL, 0, NULL, 0}
};

static void usage(char *progname)
{
    printf("%s (compiled %s)\n", progname, __DATE__);
    printf("Usage: %s [OPTION]\n"
        " -i, --image <filename>    : Software to be installed\n"
        " -v, --verbose             : Print extra information\n"
        " -c, --check               : Check update archive\n"
        " -h, --help                : Print this help and exit\n",
        progname);
}

static void swupgrade_init(struct swupgrade_cfg *swcfg)
{
    memset(swcfg, 0, sizeof(struct swupgrade_cfg));
    LIST_INIT(&swcfg->images);
}

static int swupgrade_check_install_file(int fd, struct swupgrade_cfg *swcfg)
{
    int pos = 0, count = 0;
    struct image *img;

    gpio_set_target(swcfg);

    if ((pos = cpio_utils_extract_sw_description(fd, SW_DESCRIPTION_FILENAME, 0)) < 0) {
        return -1;
    }

    if (parser_parse_cfg(swcfg, SW_DESCRIPTION_PATH) < 0) {
        return -1;
    }

    // verify each image in the config is in the archive
    LIST_FOREACH(img, &swcfg->images, next) {
        if (cpio_utils_find_img(fd, img, pos) != 0) {
            printf("image %s not found, cannot continue! \n\n", img->filename);
            return -1;
        }
        count++;
        //printf("found %s at pos %d with size %d \n\n", img->filename, img->offset, img->size);
    }
    swcfg->image_count = count;

    return 0;
}

static int swupgrade_install_from_file(int fd)
{
    int count = 0, step = 0, progress = 0;
    struct swupgrade_cfg swcfg;
    struct image *img;

    swupgrade_init(&swcfg);

    if (swupgrade_check_install_file(fd, &swcfg) != 0) {
        printf("Software Upgrade check failed \n");
        return -1;
    }

    // probe MTD if this update targets MTD's
    if (swcfg.target == TARGET_MTD) {
        if(flash_init() != 0) {
            printf("Flash init failed! \n");
            return -1;
        }
    }
    
    step = (100 / swcfg.image_count);
    struct image_handler *handler;
    LIST_FOREACH(img, &swcfg.images, next) {
        //printf("image: %s (%d), device: %s \n", img->filename, img->type, img->device);
        if ((handler = handler_find(img)) == NULL) {
            printf("Image handler for type %d not found, skipping \n", img->type);
            continue;
        }
        
        //TODO: hmmm, not sure I like assignig the CPIO archive fd here
        img->src_fd = fd;
        notifier_notify(img->filename, progress);
        handler->installer(img, NULL);
        progress += step;
        notifier_notify(img->filename, progress);
    }

    notifier_notify("Update complete, rebooting", 100);
    parser_cleanup(&swcfg, SW_DESCRIPTION_PATH);

    return 0;
}

int main(int argc, char **argv)
{
    int c;
    int opt_i = 0;
    int opt_v = 0;
    int opt_c = 0;
    int fd;
    int ret = 0;
    char fname[MAX_IMAGE_FNAME];
	struct stat sb;

    printf("Reach Software Upgrade \n");

    if (argc < 2) {
        usage(argv[0]);
        exit(0);
    }

    while (1) {
        int c = getopt_long(argc, argv, "cvhi:?", long_options, 0);

        if (c == -1) {
            break;
        }

        switch (c) {
            case 'i':
                strncpy(fname, optarg, sizeof(fname));
                opt_i = 1;
                break;
            case 'v':
                opt_v = 1;
                break;
            case 'c':
                opt_c = 1;
                break;
            case 'h':
                usage(argv[0]);
                exit(0);
                break;
            default:
                usage(argv[0]);
                exit(0);
                break;
        }
    }

    if (opt_i != 1) {
		printf("Update image file required! \n\n");
        usage(argv[0]);
		exit(1);
    }

	if (stat(fname, &sb) == -1) {
		printf("Update file does not exist, please reboot! \n");
		exit(1);
	}

	if ((fd = open(fname, O_RDONLY)) < 0) {
		printf("Error reading file, please reboot! \n");
		exit(1);
	}

    if (opt_c == 1) {
        struct swupgrade_cfg swcfg;
        struct image *img;
        
        swupgrade_init(&swcfg);

        ret = swupgrade_check_install_file(fd, &swcfg);
        if (ret == 0) {
            printf("Update version: %s\n\n", swcfg.version);
            
            printf("Images available in archive:\n");
            LIST_FOREACH(img, &swcfg.images, next) {
                printf("\t%s\n", img->filename);
            }
            printf("\nSoftware image verified\n\n");
        }
        parser_cleanup(&swcfg, SW_DESCRIPTION_PATH);
    } else {
        ret = swupgrade_install_from_file(fd);
    }

	if (fd) {
        close(fd);
    }

    return ret;
}
