/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <mtd/mtd-user.h>

#include "common.h"
#include "utils.h"
#include "handlers.h"

void handler_ubifs(void);

static int handler_ubifs_image(struct image *img, void __attribute__ ((__unused__)) *data)
{
    int fdout, ret;
    unsigned long offset = 0;

    // we have to have a volume name and a mtdnum
    if (img->volume == NULL || img->mtdnum < 0) {
        printf("UBIFS image invalid, missing mtdnum or volume \n");
        return -1;
    }

    printf("Installing %s to mtd%d on %s of size %d starting at %d \n", img->filename, 
            img->mtdnum, img->volume, img->src_size, img->src_offset);

    fdout = flash_ubi_init_update(img->mtdnum, img->volume, img->src_size);
    if (fdout < 0) {
        printf("UBI init update failed \n");
        return -1;
    }

    if (lseek(img->src_fd, img->src_offset, SEEK_SET) == -1) {
		printf("Unable to seek offet on CPIO archive : %s\n", strerror(errno));
	} else {
        ret = utils_copyfile(img->src_fd, fdout, img->src_size, &offset);
        if (ret != 0) {
            printf("Error writing file %s \n", img->filename);
        }
    }

    close(fdout);

    return 0;
}

__attribute__((constructor))
void handler_ubifs(void) {
    handler_register("ubifs", handler_ubifs_image, NULL);
}
