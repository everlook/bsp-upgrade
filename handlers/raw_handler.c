/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "common.h"
#include "cpio_utils.h"
#include "handlers.h"

#define MMC_MOUNT_DIR "/tmp/mmc/"

void handler_raw(void);

static int handler_raw_openfile(const struct image *img)
{
    int ret;
    //off_t size;
	char output_file[64];

    if (img->type == IMAGE_TYPE_VFAT) {
        if ((ret = (mkdir(MMC_MOUNT_DIR, S_IRWXU))) == -1) {
            printf("Error creating %s - %s \n", MMC_MOUNT_DIR, strerror(errno));
            return -1;
        }
        if ((ret = mount(img->device, MMC_MOUNT_DIR, "vfat", 0, NULL)) == -1) {
            printf("Error mounting %s - %s \n", img->device, strerror(errno));
            return -1;
        }

	    strncpy(output_file, MMC_MOUNT_DIR, sizeof(output_file));
	    strcat(output_file, img->filename);

	    ret = open(output_file, O_CREAT | O_WRONLY | O_TRUNC,  S_IRUSR | S_IWUSR );
        if (ret < 0) {
		    printf("I cannot open %s - %s \n", output_file, strerror(errno));
            return -1;
        }

        return ret;
    }

    if (img->type == IMAGE_TYPE_RAW) {
        ret = open(img->device, O_WRONLY);
        if (ret < 0) {
	        printf("I cannot open %s - %s \n", img->device, strerror(errno));
            return -1;
        }

        //TODO: may want to erase here
        //size = lseek(ret, 0, SEEK_END);
        //printf("Size of raw block device: %d \n", size);
        //size = lseek(ret, 0, SEEK_SET);

        return ret;
    }

    printf("Image type %d not supported \n", img->type);

    return -1;
}

static void handler_raw_cleanup(const struct image *img)
{
    int ret;

    if (img->type == IMAGE_TYPE_VFAT) {
        ret = umount(MMC_MOUNT_DIR);
        if (ret == -1) {
            printf("Error unmounting - %s \n", strerror(errno));
        }

        ret = rmdir(MMC_MOUNT_DIR);
        if (ret == -1) {
            printf("Error rmdir - %s \n", strerror(errno));
        }
    }
}

static int handler_raw_image(struct image *img, void __attribute__ ((__unused__)) *data)
{
    int fdout, ret;
    unsigned long offset = 0;

    printf("Installing %s to %s of size %d starting at %d \n", img->filename, 
            img->device, img->src_size, img->src_offset);

    if ((fdout = handler_raw_openfile(img)) == -1) {
        printf("Error opening file for device %s \n", img->device);
        return -1;
    }

    if (lseek(img->src_fd, img->src_offset, SEEK_SET) == -1) {
		printf("Unable to seek offet on CPIO archive : %s\n", strerror(errno));
	} else {
        ret = utils_copyfile(img->src_fd, fdout, img->src_size, &offset);
        if (ret != 0) {
            printf("Error writing file %s \n", img->filename);
        }
    }

    if (fdout) {
        close(fdout);
    }

    handler_raw_cleanup(img);

    return 0;
}

__attribute__((constructor))
void handler_raw(void) {
    handler_register("raw", handler_raw_image, NULL);
    handler_register("vfat", handler_raw_image, NULL);
}

