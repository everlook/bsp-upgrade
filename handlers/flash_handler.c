/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <mtd/mtd-user.h>

#include "common.h"
#include "utils.h"
#include "handlers.h"

void handler_flash(void);

static int handler_flash_image(struct image *img, void __attribute__ ((__unused__)) *data)
{
    int ret;

    printf("************************\n");
    printf("Installing %s to mtd%d of size %lld starting at %d \n", img->filename, 
            img->mtdnum, img->src_size, img->src_offset);
    
    ret = flash_nand_init_update(img);
    if (ret < 0) {
        printf("NAND init failed \n");
        return -1;
    }
    printf("************************\n\n");

    return 0;
}

__attribute__((constructor))
void handler_flash(void) {
    handler_register("nand", handler_flash_image, NULL);
}
