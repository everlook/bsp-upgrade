/*
 * (C) Copyright 2018
 * Jeff Horn, Reach Technology, jeff.horn@reachtech.com.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "common.h"
#include "utils.h"
#include "handlers.h"

#define MAX_HANDLERS 8
struct image_handler supported_handlers[MAX_HANDLERS];
static unsigned int nr_handlers = 0;

int handler_register(const char *name, handler installer, void *data)
{
    if (nr_handlers > MAX_HANDLERS - 1) {
        return -1;
    }
    
    strncpy(supported_handlers[nr_handlers].name, name, 
            sizeof(supported_handlers[nr_handlers].name));
    supported_handlers[nr_handlers].installer = installer;
    supported_handlers[nr_handlers].data = data;

    nr_handlers++;

    return 0;
}

struct image_handler *handler_find(struct image *img)
{
    int i;

    for (i = 0; i < nr_handlers; i++) {
        
        //TODO: maybe get rid of the type enum and just use strcmp
        if (img->type == utils_covert_to_image_type(supported_handlers[i].name)) {
            return &supported_handlers[i];
        }
    }
    return NULL;
}

void handler_print_regisered(void)
{
    unsigned int i;

    if (nr_handlers == 0) {
        return;
    }
        
    printf("Registered Image Handlers \n");
    for (i = 0; i < nr_handlers; i++) {
        printf("\t%s \n", supported_handlers[i].name);
    }
}
